package italdea.igaleria;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;

import java.io.File;

import italdea.igaleria.adapters.karpetaAdapters.AbestiKarpetaAdapter;
import italdea.igaleria.adapters.karpetaAdapters.ArgazkiKarpetaAdapter;
import italdea.igaleria.adapters.karpetaAdapters.BideoKarpetaAdapter;
import italdea.igaleria.adapters.thumbnailAdapters.AbestiakAdapter;
import italdea.igaleria.adapters.thumbnailAdapters.ArgazkiakAdapter;
import italdea.igaleria.adapters.thumbnailAdapters.BideoakAdapter;
import italdea.igaleria.adapters.thumbnailEragiketak.ArgazkiaPantailaratu;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, FitxategiakBilatu.AsyncResponse {
    private ArgazkiakAdapter argazkiaAdapter;
    private ArgazkiKarpetaAdapter argazkiKarpetaAdapter;
    private BideoakAdapter bideoakAdapter;
    private BideoKarpetaAdapter bideoKarpetaAdapter;
    private AbestiakAdapter abestiakAdapter;
    private AbestiKarpetaAdapter abestiKarpetaAdapter;
    private GridView gridview;
    private SharedPreferences sharedPref;
    private AdapterView.OnItemClickListener karpetaOnClickListener;
    private MenuItem tamaina;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && isExternalStorageReadable()) {
                MultimediaLista.getMultimediaLista().argazkiakKargatu(this);
            } else
                //NO SE PUEDE LEER O LO QUE SEA, INICIALIZAMOS LA LISTA
                MultimediaLista.getMultimediaLista();
        }
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        sharedPref = getSharedPreferences("ezarpenak", Context.MODE_PRIVATE);
        if (!sharedPref.contains("tamaina")) {
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("tamaina", "txikia");
            editor.apply();
        }


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        gridview = (GridView) findViewById(R.id.gridview);

        argazkiKarpetaAdapter = new ArgazkiKarpetaAdapter(this);

        gridview.setAdapter(argazkiKarpetaAdapter);
        final Context c = this;

        final AdapterView.OnItemClickListener thumbnailOnClickListener = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                if (gridview.getAdapter() == argazkiaAdapter) {
                    Intent i = new Intent(c, ArgazkiaPantailaratu.class);
                    i.putExtra("karpetaPos", argazkiaAdapter.getKarpetarenPosizioa());
                    i.putExtra("argazkiPos", position);
                    startActivity(i);
                } else if (gridview.getAdapter() == abestiakAdapter) {
                    //ABRIR REPRODUCTOR DE MUSICA
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    File file = new File(MultimediaLista.getMultimediaLista().getAbestienKarpeta(abestiakAdapter.getKarpetarenPosizioa()).getMultimedia(position).getPath());
                    if (Build.VERSION.SDK_INT > 23) {
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        intent.setDataAndType(FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", file), "audio/*");
                    } else
                        intent.setDataAndType(Uri.fromFile(file), "audio/*");

                    startActivity(intent);
                } else if (gridview.getAdapter() == bideoakAdapter) {
                    //ABRIR REPRODUCTOR DE VIDEOS
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    File file = new File(MultimediaLista.getMultimediaLista().getBideoenKarpeta(bideoakAdapter.getKarpetarenPosizioa()).getMultimedia(position).getPath());

                    if (Build.VERSION.SDK_INT > 23) {
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        intent.setDataAndType(FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", file), "video/*");
                    } else
                        intent.setDataAndType(Uri.fromFile(file), "video/*");

                    startActivity(intent);
                }

            }
        };

        karpetaOnClickListener = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                if (gridview.getAdapter() == argazkiKarpetaAdapter) {
                    argazkiaAdapter = new ArgazkiakAdapter(c, position);
                    gridview.setAdapter(argazkiaAdapter);
                } else if (gridview.getAdapter() == abestiKarpetaAdapter) {
                    abestiakAdapter = new AbestiakAdapter(c, position);
                    gridview.setAdapter(abestiakAdapter);
                } else if (gridview.getAdapter() == bideoKarpetaAdapter) {
                    bideoakAdapter = new BideoakAdapter(c, position);
                    gridview.setAdapter(bideoakAdapter);
                }
                gridview.setOnItemClickListener(thumbnailOnClickListener);
            }
        };

        gridview.setOnItemClickListener(karpetaOnClickListener);
        if (savedInstanceState != null) {
            String oraingoa = savedInstanceState.getString("class");
            int karpeta = savedInstanceState.getInt("pos");
            if (oraingoa.equals(ArgazkiakAdapter.class.getSimpleName())) {
                argazkiaAdapter = new ArgazkiakAdapter(this, karpeta);
                gridview.setAdapter(argazkiaAdapter);
                gridview.setOnItemClickListener(thumbnailOnClickListener);
            } else if (oraingoa.equals(AbestiakAdapter.class.getSimpleName())) {
                abestiakAdapter = new AbestiakAdapter(this, karpeta);
                gridview.setAdapter(abestiakAdapter);
                gridview.setOnItemClickListener(thumbnailOnClickListener);
            } else if (oraingoa.equals(AbestiKarpetaAdapter.class.getSimpleName())) {
                abestiKarpetaAdapter = new AbestiKarpetaAdapter(this);
                gridview.setAdapter(abestiKarpetaAdapter);
                gridview.setOnItemClickListener(karpetaOnClickListener);
            } else if (oraingoa.equals(BideoakAdapter.class.getSimpleName())) {
                bideoakAdapter = new BideoakAdapter(this, karpeta);
                gridview.setAdapter(bideoakAdapter);
                gridview.setOnItemClickListener(thumbnailOnClickListener);
            } else if (oraingoa.equals(BideoKarpetaAdapter.class.getSimpleName())) {
                bideoKarpetaAdapter = new BideoKarpetaAdapter(this);
                gridview.setAdapter(bideoKarpetaAdapter);
                gridview.setOnItemClickListener(karpetaOnClickListener);
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (gridview.getAdapter().getClass().equals(ArgazkiakAdapter.class)) {
            gridview.setAdapter(argazkiKarpetaAdapter);
            gridview.setOnItemClickListener(karpetaOnClickListener);
        } else if (gridview.getAdapter().getClass().equals(BideoakAdapter.class)) {
            gridview.setAdapter(bideoKarpetaAdapter);
            gridview.setOnItemClickListener(karpetaOnClickListener);
        } else if (gridview.getAdapter().getClass().equals(AbestiakAdapter.class)) {
            gridview.setAdapter(abestiKarpetaAdapter);
            gridview.setOnItemClickListener(karpetaOnClickListener);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.refresh, menu);
        getMenuInflater().inflate(R.menu.main, menu);
        String tamainaString = sharedPref.getString("tamaina", "txikia");
        tamaina = menu.getItem(1);
        if (tamainaString.equals("txikia"))
            tamaina.setTitle(getString(R.string.handia));
        else
            tamaina.setTitle(getString(R.string.txikia));

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify argazkiKarpetaAdapter parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_refresh) {
            Class<? extends ListAdapter> mota = gridview.getAdapter().getClass();
            if (mota.equals(ArgazkiKarpetaAdapter.class)) {
                MultimediaLista.getMultimediaLista().argazkiakKargatu(this);
            } else if (mota.equals(AbestiKarpetaAdapter.class)) {
                MultimediaLista.getMultimediaLista().abestiakKargatu(this);
            } else if (mota.equals(BideoKarpetaAdapter.class)) {
                MultimediaLista.getMultimediaLista().bideoakKargatu(this);
            }
        }
        if (id == R.id.action_switch_size) {
            String tamainaString = sharedPref.getString("tamaina", "txikia");
            if (tamainaString.equals("txikia")) {
                tamaina.setTitle(getString(R.string.handia));
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("tamaina", "handia");
                editor.apply();
            } else {
                tamaina.setTitle(getString(R.string.txikia));
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("tamaina", "txikia");
                editor.apply();
            }
        }
        invalidateOptionsMenu();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("class", gridview.getAdapter().getClass().getSimpleName());
        int pos = -1;
        if (gridview.getAdapter() == argazkiaAdapter) {
            pos = argazkiaAdapter.getKarpetarenPosizioa();
        } else if (gridview.getAdapter() == abestiakAdapter) {
            pos = abestiakAdapter.getKarpetarenPosizioa();
        } else if (gridview.getAdapter() == bideoakAdapter) {
            pos = bideoakAdapter.getKarpetarenPosizioa();
        }
        outState.putInt("pos", pos);

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_gallery) {

            MultimediaLista.getMultimediaLista().argazkiakKargatu(this);
            if (argazkiKarpetaAdapter == null)
                argazkiKarpetaAdapter = new ArgazkiKarpetaAdapter(this);
            gridview.setAdapter(argazkiKarpetaAdapter);

        } else if (id == R.id.nav_audio_gallery) {

            MultimediaLista.getMultimediaLista().abestiakKargatu(this);
            if (abestiKarpetaAdapter == null)
                abestiKarpetaAdapter = new AbestiKarpetaAdapter(this);
            gridview.setAdapter(abestiKarpetaAdapter);

        } else if (id == R.id.nav_video_gallery) {

            MultimediaLista.getMultimediaLista().bideoakKargatu(this);
            if (bideoKarpetaAdapter == null)
                bideoKarpetaAdapter = new BideoKarpetaAdapter(this);
            gridview.setAdapter(bideoKarpetaAdapter);
        }

        gridview.setOnItemClickListener(karpetaOnClickListener);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
    }


    @Override
    public void processFinish(int output, String mota) {
        Log.d("MOTA", mota);
        switch (mota.toLowerCase()) {
            case "bideoak":
                if (bideoakAdapter != null)
                    bideoakAdapter.notifyDataSetChanged();
                bideoKarpetaAdapter.notifyDataSetChanged();
                break;
            case "argazkiak":
                if (argazkiaAdapter != null)
                    argazkiaAdapter.notifyDataSetChanged();
                argazkiKarpetaAdapter.notifyDataSetChanged();
                break;
            case "abestiak":
                if (abestiakAdapter != null)
                    abestiakAdapter.notifyDataSetChanged();
                abestiKarpetaAdapter.notifyDataSetChanged();
                break;
            default:
                break;
        }

    }
}

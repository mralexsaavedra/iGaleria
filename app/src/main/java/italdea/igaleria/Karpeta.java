package italdea.igaleria;

import java.util.ArrayList;

import italdea.igaleria.multimedia.IMultimedia;

public class Karpeta<E extends IMultimedia> {
    private ArrayList<E> multimedia;
    private String karpetaIzena;

    public Karpeta(String karpetaIzena) {
        multimedia = new ArrayList<>();
        this.karpetaIzena = karpetaIzena;
    }

    public E getMultimedia(int zein) {
        return multimedia.get(zein);
    }

    public String getKarpetaIzena() {
        return karpetaIzena;
    }

    public void multimediaGehitu(E sartzeko) {
        multimedia.add(sartzeko);
    }

    public int getTamaina() {
        return multimedia.size();
    }
}

package italdea.igaleria;

import android.content.Context;

import java.util.ArrayList;

import italdea.igaleria.multimedia.Abestia;
import italdea.igaleria.multimedia.Argazkia;
import italdea.igaleria.multimedia.Bideoa;
import italdea.igaleria.multimedia.IMultimedia;

public class MultimediaLista {
    private static MultimediaLista gureMultimediaLista;
    private ArrayList<Karpeta<Argazkia>> karpetakArgazkiak;
    private ArrayList<Karpeta<Abestia>> abestiakKarpetak;
    private ArrayList<Karpeta<Bideoa>> bideoakKarpetak;

    private MultimediaLista() {
        this.karpetakArgazkiak = new ArrayList<>();
        this.abestiakKarpetak = new ArrayList<>();
        this.bideoakKarpetak = new ArrayList<>();

    }

    public static MultimediaLista getMultimediaLista() {
        return gureMultimediaLista == null ? gureMultimediaLista = new MultimediaLista() : gureMultimediaLista;
    }

    public Karpeta<Argazkia> getArgazkienKarpeta(int posizioa) {
        return karpetakArgazkiak.get(posizioa);
    }

    public Karpeta<Abestia> getAbestienKarpeta(int posizioa) {
        return abestiakKarpetak.get(posizioa);
    }

    public Karpeta<Bideoa> getBideoenKarpeta(int posizioa) {
        return bideoakKarpetak.get(posizioa);
    }


    public void argazkiKarpetaSartu(Karpeta<Argazkia> sartzeko) {
        karpetakArgazkiak.add(sartzeko);
    }

    public void abestiKarpetaSartu(Karpeta<Abestia> sartzeko) {
        abestiakKarpetak.add(sartzeko);
    }

    public void bideoKarpetaSartu(Karpeta<Bideoa> sartzeko) {
        bideoakKarpetak.add(sartzeko);
    }


    public int getArgazkiKarpetaKopurua() {
        return karpetakArgazkiak.size();
    }

    public int getBideoKarpetaKopurua() {
        return bideoakKarpetak.size();
    }

    public int getAbestiKarpetaKopurua() {
        return abestiakKarpetak.size();
    }

    public Karpeta<Argazkia> argazkiKarpetaBilatu(String karpetaIzena) {
        return (Karpeta<Argazkia>) this.bilatuKarpeta(karpetaIzena, this.karpetakArgazkiak);
    }

    public Karpeta<Bideoa> bideoKarpetaBilatu(String karpetaIzena) {
        return (Karpeta<Bideoa>) this.bilatuKarpeta(karpetaIzena, this.bideoakKarpetak);
    }

    public Karpeta<Abestia> abestiKarpetaBilatu(String karpetaIzena) {
        return (Karpeta<Abestia>) this.bilatuKarpeta(karpetaIzena, this.abestiakKarpetak);
    }


    private Karpeta<? extends IMultimedia> bilatuKarpeta(String karpetaIzena, ArrayList<? extends Karpeta> biltzeko) {
        for (int i = 0; i < biltzeko.size(); i++)
            if (karpetaIzena.equals(biltzeko.get(i).getKarpetaIzena()))
                return biltzeko.get(i);
        return null;
    }

    public void argazkiakKargatu(Context c) {
        karpetakArgazkiak.clear();
        FitxategiakBilatu f = new FitxategiakBilatu();
        f.setContext(c);
        f.execute("argazkiak");
    }

    public void abestiakKargatu(Context c) {
        abestiakKarpetak.clear();
        FitxategiakBilatu f = new FitxategiakBilatu();
        f.setContext(c);
        f.execute("abestiak");
    }

    public void bideoakKargatu(Context c) {
        bideoakKarpetak.clear();
        FitxategiakBilatu f = new FitxategiakBilatu();
        f.setContext(c);
        f.execute("bideoak");
    }


}
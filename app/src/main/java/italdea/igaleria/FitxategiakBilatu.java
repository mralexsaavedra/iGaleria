package italdea.igaleria;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Log;

import italdea.igaleria.multimedia.Abestia;
import italdea.igaleria.multimedia.Argazkia;
import italdea.igaleria.multimedia.Bideoa;

public class FitxategiakBilatu extends AsyncTask<String, Void, Integer> {
    private Context context;
    private String mota;

    @Override
    protected Integer doInBackground(String... params) {
        mota = params[0];
        switch (mota.toLowerCase()) {
            case "bideoak":
                parseAllVideo();
                break;
            case "argazkiak":
                parseAllImages();
                break;
            case "abestiak":
                parseAllAudio();
                break;
            default:
                break;
        }
        return 0;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    private void parseAllVideo() {
        String[] proj = {MediaStore.Video.Media._ID,
                MediaStore.Video.Media.DATA,
                MediaStore.Video.Media.BUCKET_DISPLAY_NAME};
        String ordena = MediaStore.Video.Media.DATE_TAKEN + " DESC";
        Cursor cursor = context.getContentResolver().query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                proj, null, null, ordena);
        if (cursor != null) {
            int count = cursor.getCount();
            Log.d("BIDEO KOPURUA", String.valueOf(count));
            Karpeta<Bideoa> oraingoa = null;
            while (cursor.moveToNext()) {
                long id = cursor.getLong(cursor.getColumnIndex(MediaStore.Video.Media._ID));
                int file_ColumnIndex = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
                String bucket = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME));
                if (oraingoa == null || !oraingoa.getKarpetaIzena().equals(bucket)) {
                    oraingoa = MultimediaLista.getMultimediaLista().bideoKarpetaBilatu(bucket);
                    if (oraingoa == null) {
                        oraingoa = new Karpeta<>(bucket);
                        MultimediaLista.getMultimediaLista().bideoKarpetaSartu(oraingoa);
                    }
                }

                int video_column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
                String name = cursor.getString(video_column_index);
                String filepath = cursor.getString(file_ColumnIndex);
                oraingoa.multimediaGehitu(new Bideoa(name, filepath, id, context));
                Log.d("SARTUTAKO BIDEO KOPURUA", String.valueOf(MultimediaLista.getMultimediaLista().getBideoKarpetaKopurua()));
            }
            cursor.close();
        }

        Log.d("SARTUTAKO BIDEO KOPURUA", String.valueOf(MultimediaLista.getMultimediaLista().getBideoKarpetaKopurua()));
    }

    private void parseAllImages() {

        String[] projection = {MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID, MediaStore.Images.Media.BUCKET_DISPLAY_NAME};
        String ordena = MediaStore.Images.Media.DATE_TAKEN + " DESC";
        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, null, ordena);
        if (cursor != null) {
            Karpeta<Argazkia> oraingoa = null;
            while (cursor.moveToNext()) {
                long id = cursor.getLong(cursor.getColumnIndex(MediaStore.Images.Media._ID));
                int file_ColumnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                String bucket = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME));

                if (oraingoa == null || !oraingoa.getKarpetaIzena().equals(bucket)) {
                    oraingoa = MultimediaLista.getMultimediaLista().argazkiKarpetaBilatu(bucket);
                    if (oraingoa == null) {
                        oraingoa = new Karpeta<>(bucket);
                        MultimediaLista.getMultimediaLista().argazkiKarpetaSartu(oraingoa);
                    }
                }
                String path = cursor.getString(file_ColumnIndex);

                String fileName = path.substring(path.lastIndexOf("/") + 1, path.length());

                oraingoa.multimediaGehitu(new Argazkia(fileName, path, id, context));
            }
            cursor.close();
        }
    }

    @Override
    protected void onPostExecute(Integer integer) {
        ((AsyncResponse) context).processFinish(integer, mota);
    }

    private void parseAllAudio() {

        String ordena = MediaStore.Audio.Media.ALBUM + " DESC";
        Cursor cursor = context.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, null, null, ordena);

        if (cursor != null && cursor.moveToFirst()) {
            Log.d("ABESTI KOP", String.valueOf(cursor.getCount()));
            Karpeta<Abestia> albuma = null;
            do {

                int titleColumn = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
                int albumColumn = cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM);
                int filePathIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                String album = cursor.getString(albumColumn);
                if (albuma == null || !albuma.getKarpetaIzena().equals(album)) {
                    albuma = MultimediaLista.getMultimediaLista().abestiKarpetaBilatu(album);
                    if (albuma == null) {
                        albuma = new Karpeta<>(album);
                        MultimediaLista.getMultimediaLista().abestiKarpetaSartu(albuma);
                    }
                }
                String titulua = cursor.getString(titleColumn);
                String path = cursor.getString(filePathIndex);
                albuma.multimediaGehitu(new Abestia(titulua, path, context));

            } while (cursor.moveToNext());
            cursor.close();
        }
    }

    public interface AsyncResponse {
        void processFinish(int output, String mota);
    }
}

package italdea.igaleria.multimedia;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import italdea.igaleria.R;

public class Abestia implements IMultimedia {
    private String path;
    private String izena;
    private Context context;


    public Abestia(String izena, String path, Context context) {
        this.path = path;
        this.izena = izena;
        this.context = context;
    }

    @Override
    public String getIzena() {
        return izena;
    }

    @Override
    public String getPath() {
        return path;
    }

    public Drawable getThumbnail() {
        return ContextCompat.getDrawable(context, R.drawable.ic_music_note_black_24px);
    }
}
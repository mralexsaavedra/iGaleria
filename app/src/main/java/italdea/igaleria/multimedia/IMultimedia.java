package italdea.igaleria.multimedia;

import android.graphics.drawable.Drawable;

public interface IMultimedia {
    String getIzena();

    String getPath();

    Drawable getThumbnail();
}

package italdea.igaleria.multimedia;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.provider.MediaStore;

public class Bideoa implements IMultimedia {
    private String path;
    private String izena;
    private long id;
    private Context context;
    private SharedPreferences sharedPrefs;


    public Bideoa(String izena, String path, long id, Context context) {
        this.path = path;
        this.izena = izena;
        this.id = id;
        this.context = context;
        this.sharedPrefs = context.getSharedPreferences("ezarpenak", Context.MODE_PRIVATE);
    }

    @Override
    public String getIzena() {
        return izena;
    }

    @Override
    public String getPath() {
        return path;
    }

    public Drawable getThumbnail() {
        int thumbnailTamaina;
        if (sharedPrefs.getString("tamaina", "txikia").equals("txikia")) {
            thumbnailTamaina = MediaStore.Video.Thumbnails.MICRO_KIND;
        } else {
            thumbnailTamaina = MediaStore.Video.Thumbnails.MINI_KIND;
        }
        return new BitmapDrawable(Resources.getSystem(), MediaStore.Video.Thumbnails.getThumbnail(
                context.getContentResolver(), id, thumbnailTamaina, null));

    }
}

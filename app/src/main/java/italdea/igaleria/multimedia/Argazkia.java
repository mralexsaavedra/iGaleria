package italdea.igaleria.multimedia;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.provider.MediaStore;
import android.util.Log;

public class Argazkia implements IMultimedia {
    private String path;
    private String izena;
    private long id;
    private Context context;
    private SharedPreferences sharedPrefs;

    public Argazkia(String izena, String path, long id, Context context) {
        this.path = path;
        this.izena = izena;
        this.id = id;
        this.context = context;
        this.sharedPrefs = context.getSharedPreferences("ezarpenak", Context.MODE_PRIVATE);
    }

    @Override
    public String getIzena() {
        return izena;
    }

    @Override
    public String getPath() {
        return path;
    }

    public Bitmap getArgazkia() {
        Bitmap argazkia = BitmapFactory.decodeFile(path);
        Log.d("TAMAINA ORIGINALA", argazkia.getWidth() + " --- " + argazkia.getHeight());
        if (argazkia.getHeight() > 3072 || argazkia.getWidth() > 3072) {
            double heigth = argazkia.getHeight();
            double width = argazkia.getWidth();
            double maxHeigth = 3072;
            double maxWidth = 3072;
            if (width > heigth) {
                double ratio = width / maxWidth;
                Log.d("TAMAINA RATIO", String.valueOf(ratio));
                width = maxWidth;
                heigth = (int) (heigth / ratio);
            } else if (width < heigth) {
                double ratio = heigth / maxHeigth;
                width = (int) (width / ratio);
                heigth = maxHeigth;
            }
            Log.d("TAMAINA WIDTH", String.valueOf(width));
            Log.d("TAMAINA HEIGHT", String.valueOf(heigth));

            argazkia = Bitmap.createScaledBitmap(argazkia, (int) width, (int) heigth, true);
            Log.d("TAMAINA BERRIA", argazkia.getWidth() + " --- " + argazkia.getHeight());
        }
        return argazkia;
    }

    public Drawable getThumbnail() {
        int thumbnailTamaina;
        if (sharedPrefs.getString("tamaina", "txikia").equals("txikia")) {
            thumbnailTamaina = MediaStore.Images.Thumbnails.MICRO_KIND;
        } else {
            thumbnailTamaina = MediaStore.Images.Thumbnails.MINI_KIND;
        }
        return new BitmapDrawable(Resources.getSystem(), MediaStore.Images.Thumbnails.getThumbnail(
                context.getContentResolver(), id, thumbnailTamaina, null));

    }

}

package italdea.igaleria.adapters.thumbnailAdapters;

import android.content.Context;
import android.graphics.Point;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import italdea.igaleria.MultimediaLista;

public class ArgazkiakAdapter extends BaseAdapter {
    private Context mContext;
    private int karpetaPosizioa;
    private int tamaina;

    public ArgazkiakAdapter(Context c, int zeinKarpeta) {
        mContext = c;
        this.karpetaPosizioa = zeinKarpeta;
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        Point size = new Point();
        wm.getDefaultDisplay().getSize(size);
        tamaina = (int) (size.x * 0.45);
    }

    public int getCount() {
        return MultimediaLista.getMultimediaLista().getArgazkienKarpeta(karpetaPosizioa).getTamaina();
    }

    public Object getItem(int position) {
        return MultimediaLista.getMultimediaLista().getArgazkienKarpeta(karpetaPosizioa).getMultimedia(position).getThumbnail();
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;

        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(tamaina, tamaina));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(0, 0, 0, 0);
        } else {
            imageView = (ImageView) convertView;
        }
        imageView.setImageDrawable(MultimediaLista.getMultimediaLista().getArgazkienKarpeta(karpetaPosizioa).getMultimedia(position).getThumbnail());
        return imageView;
    }

    public int getKarpetarenPosizioa() {
        return karpetaPosizioa;
    }
}

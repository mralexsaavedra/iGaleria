package italdea.igaleria.adapters.thumbnailAdapters;

import android.content.Context;
import android.graphics.Point;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import italdea.igaleria.MultimediaLista;
import italdea.igaleria.R;

public class AbestiakAdapter extends BaseAdapter {
    private Context mContext;
    private int karpetaPosizioa;
    private int tamaina;

    public AbestiakAdapter(Context c, int zeinKarpeta) {
        mContext = c;
        this.karpetaPosizioa = zeinKarpeta;
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        Point size = new Point();
        wm.getDefaultDisplay().getSize(size);
        tamaina = (int) (size.x * 0.45);
    }

    public int getCount() {
        return MultimediaLista.getMultimediaLista().getAbestienKarpeta(karpetaPosizioa).getTamaina();
    }

    public Object getItem(int position) {
        return MultimediaLista.getMultimediaLista().getAbestienKarpeta(karpetaPosizioa).getMultimedia(position).getThumbnail();
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        View v;
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.row, parent, false);
            TextView label = (TextView) v.findViewById(R.id.image_name);
            ImageView imageView = (ImageView) v.findViewById(R.id.album_image);
            v.setLayoutParams(new LinearLayout.LayoutParams(tamaina, tamaina));
            imageView.setMaxHeight((int) (tamaina * 0.8));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(0, 0, 0, 0);

            AbestiakAdapter.ViewHolder holder = new AbestiakAdapter.ViewHolder();
            holder.text = label;
            holder.image = imageView;
            v.setTag(holder);
        } else {
            v = convertView;
        }
        AbestiakAdapter.ViewHolder holder = (AbestiakAdapter.ViewHolder) v.getTag();
        holder.image.setImageDrawable(MultimediaLista.getMultimediaLista().getAbestienKarpeta(karpetaPosizioa).getMultimedia(position).getThumbnail());
        holder.text.setText(MultimediaLista.getMultimediaLista().getAbestienKarpeta(karpetaPosizioa).getMultimedia(position).getIzena());
        return v;
    }

    public int getKarpetarenPosizioa() {
        return karpetaPosizioa;
    }

    private static class ViewHolder {
        TextView text;
        ImageView image;
    }
}
